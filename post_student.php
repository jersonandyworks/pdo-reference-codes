<?php
if(isset($_POST['add_student'])){
	// Connect to database
	$db = new PDO("mysql:host=localhost;dbname=gradebook","root","");
	// prepare a query statement
	//$query = $db->prepare("INSERT INTO students SET firstname = ?, lastname = ?");
	$query = $db->prepare("INSERT INTO students SET firstname = :fname, 
		                  lastname = :lname, sex = :sex");

	// Bind the parameters
	// $query->bindParam(1,$firstname);
	// $query->bindParam(2,$lastname);

	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$sex = $_POST['gender'];
	$execute_query = [':fname' => $firstname,':lname' => $lastname,':sex' => $sex ];
	// execute the query
	if($query->execute($execute_query)){
		echo "Success!";
	}else{
		echo "please try again!";
	}
}else{
	header('Location:index.html');
}


 ?>