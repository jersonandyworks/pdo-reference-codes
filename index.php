<?php session_start();?>
<html lang="en">
<head>
	<title>PHP HTTP Methods</title>
</head>
<body>
	<?php if (isset($_SESSION['username']))	:?>
	<h3>ADD NEW STUDENT TO GRADEBOOK</h3>
	<form action="post_student.php" method="POST">
		<label>First Name:</label>
		<input type="text" name="firstname"><br>
		<label>Last Name:</label>
		<input type="text" name="lastname"><br>
		<label>Sex:</label>
		<select name="gender">
			<option value="M">Male</option>
			<option value="F">Female</option>
		</select>
		<button type="submit" name="add_student">Add Student</button>
	</form>	
	<?php else: ?>
	   <h3>Please Login !</h3>
	   <a href="http://phpprojects.app/gradebook/login">Login Here</a> | 
	   <a href="http://phpprojects.app/gradebook/register">Register Here</a>
	<?php endif; ?>
</body>
</html>