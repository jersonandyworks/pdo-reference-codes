<?php 
function connect(){
	$db = new PDO("mysql:host=localhost;dbname=gradebook","root","");
	return $db; //return the connection if successful
}

function processRegistration($input_username,$input_password,$default_role = 1){
   $db = connect();
	$query = $db->prepare("INSERT INTO users SET username = ?, password = ?, role = ?");
	$query->bindParam(1,$username);
	$query->bindParam(2,$password);
	$query->bindParam(3,$role);

	$username = $input_username;
	$password = sha1($input_password);
	$role = $default_role;
	if($query->execute()){
		return true;

	}else{
		return false;
	}
}
//check user if logged in
function processLogin($uname,$pass){
	
	$db = connect();
	$query = $db->prepare("SELECT * FROM users WHERE username = ? AND password = ?");

	$query->bindParam(1,$username);
	$query->bindParam(2,$password);
	$username = $uname;
	$password = $pass;
    if($query->execute()){
        if($query->rowCount() == 1){
		return $result = $query->fetchAll();
    	}
    	else{
    		return 0;
    	}
    }
    else{
        $query->errorInfo();
    }
}

function userExists($username_input){
    $db = connect();
     $query = $db->prepare("SELECT * FROM users WHERE username = ? ");
     $query->bindParam(1,$username);
     $username = $username_input;
     //if the query has no error during run time, then execute
     if($query->execute()){
         if($query->rowCount() == 1){
             //if there is a username that is the same with the newly registered user then return true;
             //this condition will be used for checking if there is a user exists
             return true;
         }
         else{
             //if there is no username that is currently registered then return false
             return false;
         }
     }
    //if the query has an error/s then return an array of errors
     else{
         $query->errorInfo();
     }
}
?>